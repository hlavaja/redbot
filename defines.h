/*
 * Projekt: Herní strategie pro soutěž RedBot 2013 podzim
 *          http://red-bot.rhcloud.com/node/57
 * Autor:   Jan Hlaváč, hlavaja@gmail.com
 * Datum:   2013-12-02
 * Soubor:  defines.h
 * Popis:   Definice symbolických konstant a chybových kódů programu.
 */

#ifndef DEFINES_H
#define DEFINES_H


/** Chybové kódy programu */
enum terrcodes {
    ERR_OK = 0,     /**< Vše v pořádku */
    ERR_ARGS,       /**< Chybné argumenty příkazového řádku */
    ERR_FOPEN,      /**< Nepodařilo se otevřít soubor s hracím plánem */
    ERR_PLAYFIELD,  /**< Neplatný formát hracího plánu */
    ERR_MEM,        /**< Nepodařilo se alokovat potřebnou paměť */
    ERR_MODE,       /**< Neznámý režim programu */
    ERR_UNKNOWN     /**< Neznámá chyba */
};


#endif
