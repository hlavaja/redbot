#
# Projekt: Herní strategie pro soutěž RedBot 2013 podzim
#          http://red-bot.rhcloud.com/node/57
# Autor:   Jan Hlaváč, hlavaja@gmail.com
# Datum:   2013-12-01
# Soubor:  Makefile
#

FLAGS = -std=c99 -pedantic -Wall -Wextra
PROJ = daedalus

$(PROJ):$(PROJ).c playfield.c strategy.c
		gcc $(FLAGS) $(PROJ).c playfield.c strategy.c -o $(PROJ)

remake:	clean $(PROJ)

clean:
		rm -f $(PROJ) $(PROJ).tar.gz

pack:
		tar czf $(PROJ).tar.gz *.c *.h Makefile
