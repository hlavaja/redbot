/*
 * Projekt: Herní strategie pro soutěž RedBot 2013 podzim
 *          http://red-bot.rhcloud.com/node/57
 * Autor:   Jan Hlaváč, hlavaja@gmail.com
 * Datum:   2013-12-02
 * Soubor:  playfield.c
 * Popis:   Funkce určené pro práci s hracím plánem.
 */

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"
#include "playfield.h"


/** Znaky, kterýmy jsou v souboru s hracím plánem v odpovědích hráčů označeny
 *  jednotlivé akce */
const char PF_ACT_CHAR[] = {
    [ACT_FLY]    = 'L',     /**< Let */
    [ACT_PULL]   = 'T',     /**< Tažení asteroidu */
    [ACT_SHOOT]  = 'S',     /**< Střelba */
    [ACT_DEFEND] = 'B',     /**< Bránění (zapnutí štítů) */
    [ACT_ERROR]  = 'E'      /**< Chybná akce */
};


/** Znaky, kterýmy jsou v souboru s hracím plánem v odpovědích hráčů označeny
 *  jednotlivé dílčí směry, kterými jsou akce prováděny */
const char PF_DIR_CHAR[] = {
    [DIR_L]      = 'L',     /**< Vlevo */
    [DIR_R]      = 'P',     /**< Vpravo */
    [DIR_U]      = 'N',     /**< Nahoru */
    [DIR_D]      = 'D',     /**< Dolů */
};


/** Znaky, které v odpovědi programu značí jednotlivé akce */
const char ANSW_ACT_CHAR[] = {
    [ACT_FLY]       = 'l',      /**< Let */
    [ACT_PULL]      = 't',      /**< Tažení asteroidu */
    [ACT_SHOOT]     = 's',      /**< Střelba */
    [ACT_DEFEND]    = 'b'       /**< Bránění (zapnutí štítů) */
};


/** Řetězce, které v odpovědi programu značí jednotlivé směry */
const char *ANSW_DIR_STR[] = {
    [DIR_L]         = " l",     /**< Vlevo */
    [DIR_R]         = " p",     /**< Vpravo */
    [DIR_U]         = " n",     /**< Nahoru */
    [DIR_D]         = " d",     /**< Dolů */
    [DIR_LL]        = " l l",   /**< Dvě pole vlevo */
    [DIR_RR]        = " p p",   /**< Dvě pole vpravo */
    [DIR_UU]        = " n n",   /**< Dvě pole nahoru */
    [DIR_DD]        = " d d",   /**< Dvě pole dolů */
    [DIR_LU]        = " l n",   /**< Vlevo nahoru*/
    [DIR_LD]        = " l d",   /**< Vlevo dolů */
    [DIR_RU]        = " p n",   /**< Vpravo nahoru */
    [DIR_RD]        = " p d",   /**< Vpravo dolů */
    [DIR_NODIR]     = ""        /**< Žádný směr */
};


/** Inkrementy pro jednotlivé směry letu rakety. První 4 směry platí i pro
 *  tažení asteroidu */
const TIncrement DIR_INC[] = {
    [DIR_L]         = {-1,  0}, /**< Vlevo */
    [DIR_R]         = { 1,  0}, /**< Vpravo */
    [DIR_U]         = { 0, -1}, /**< Nahoru */
    [DIR_D]         = { 0,  1}, /**< Dolů */
    [DIR_LL]        = {-2,  0}, /**< Dvě pole vlevo */
    [DIR_RR]        = { 2,  0}, /**< Dvě pole vpravo */
    [DIR_UU]        = { 0, -2}, /**< Dvě pole nahoru */
    [DIR_DD]        = { 0,  2}, /**< Dvě pole dolů */
    [DIR_LU]        = {-1, -1}, /**< Vlevo nahoru*/
    [DIR_LD]        = {-1,  1}, /**< Vlevo dolů */
    [DIR_RU]        = { 1, -1}, /**< Vpravo nahoru */
    [DIR_RD]        = { 1,  1}, /**< Vpravo dolů */
    [DIR_NODIR]     = { 0, 0}   /**< Žádný směr */
};


/**
 * Načte hrací plán ze zadaného souboru a uloží jej do struktury typu
 * TPlayfield.
 * @param pfFile Soubor, ze kterého se bude hrací plán načítat.
 * @param myID ID hráče, za kterého strategie hraje.
 * @param pf Výsledný hrací plán. Hráč, za kterého strategie hraje, je v poli
 *           hráčů uložen na pozici 0, zbylí hráči jsou pak seřazeni vzestupně
 *           podle ID.
 * @return Při úspěšném načtení vrací ERR_OK, při chybném formátu souboru
 *         ERR_PLAYFIELD, při neúspěšné alokaci paměti ERR_MEM.
 */
int _readPlayfield(FILE* pfFile, unsigned char myID, TPlayfield *pf) {
    // načtení aktuálního kola a celkového počtu kol
    unsigned long tmpRound, tmpTotalRounds;
    if (fscanf(pfFile, "%lu%lu", &tmpRound, &tmpTotalRounds) != 2 ||
            tmpRound > UINT_MAX || tmpTotalRounds > UINT_MAX ||
            tmpRound > tmpTotalRounds) {
        return ERR_PLAYFIELD;
    }
    pf->round = tmpRound;
    pf->totalRounds = tmpTotalRounds;
    // načtení šířky a výšky hracího plánu
    unsigned short tmpWidth, tmpHeight;
    if (fscanf(pfFile, "%hu%hu", &tmpWidth, &tmpHeight) != 2 ||
            tmpWidth > UCHAR_MAX || tmpHeight > UCHAR_MAX ||
            tmpWidth == 0 || tmpHeight == 0) {
        return ERR_PLAYFIELD;
    }
    pf->width = tmpWidth;
    pf->height = tmpHeight;
    // načtení informací o hráčích
    bool finishedIDs[PLAYERS_CNT] = {false};    // dokončená ID
    for (unsigned char i = 0; i < PLAYERS_CNT; i++) {
        char tmp[2];
        // ID hráče
        unsigned short playerID;
        if (fscanf(pfFile, "%*[ \t\n]Hrac%hu%1[:]", &playerID, tmp) != 2 ||
                playerID == 0 || playerID > PLAYERS_CNT ||
                finishedIDs[playerID - 1] == true) {
            return ERR_PLAYFIELD;
        }
        finishedIDs[playerID - 1] = true;
        // hráč, za kterého strategie hraje, je uložen na pozici 0, zbylí hráči
        // jsou pak seřazeni vzestupně podle ID
        unsigned char dest = playerID;
        if (dest == myID) {
            dest = MY_TEAM;
        }
        else if (playerID > myID) {
            dest--;
        }
        // body
        unsigned long tmpPoints;
        if (fscanf(pfFile, "%*[ \t\n]Body:%lu", &tmpPoints) != 1 ||
                tmpPoints > UINT_MAX) {
            return ERR_PLAYFIELD;
        }
        pf->players[dest].points = tmpPoints;
        // základna
        unsigned short tmpX, tmpY;
        if (fscanf(pfFile, "%*[ \t\n]Zakladna:[%hu,%hu%1[]]", &tmpX, &tmpY,
                tmp) != 3 || tmpX > UCHAR_MAX || tmpY > UCHAR_MAX) {
            return ERR_PLAYFIELD;
        }
        pf->players[dest].base.x = tmpX;
        pf->players[dest].base.y = tmpY;
        // rakety
        if (fscanf(pfFile, "%*[ \t\n]Raket%1[y]", tmp) != 1) {
            return ERR_PLAYFIELD;
        }
        for (unsigned char j = 0; j < ROCKETS_CNT; j++) {
            if (fscanf(pfFile, ":[%hu,%hu%1[]]", &tmpX, &tmpY, tmp) != 3 ||
                    tmpX > UCHAR_MAX || tmpY > UCHAR_MAX) {
                return ERR_PLAYFIELD;
            }
            pf->players[dest].rockets[j].coords.x = tmpX;
            pf->players[dest].rockets[j].coords.y = tmpY;
        }
    }
    // načtení odpovědí hráčů
    // (veškeré položky finishedIDs mají nyní hodnotu true, při zpracovávání
    //  odpovědí jsou tak postupně nastavovány na false)
    for (unsigned char i = 0; i < PLAYERS_CNT; i++) {
        char tmp[2];
        // ID hráče
        unsigned short playerID;
        if (fscanf(pfFile, "%*[ \t\n]Odpoved%*1[ ]hrace%hu%1[:]", &playerID,
                tmp) != 2 || playerID == 0 || playerID > PLAYERS_CNT ||
                finishedIDs[playerID - 1] == false) {
            return ERR_PLAYFIELD;
        }
        finishedIDs[playerID - 1] = false;
        // hráč, za kterého strategie hraje, je uložen na pozici 0, zbylí hráči
        // jsou pak seřazeni vzestupně podle ID
        unsigned char dest = playerID;
        if (dest == myID) {
            dest = MY_TEAM;
        }
        else if (playerID > myID) {
            dest--;
        }
        // odpověď hráče (v prvním kole je prázdná)
        if (pf->round > 0) {
            // původní odpověď
            fscanf(pfFile, "(%*[^)])");
            // kanonizovaná odpověď - načtení odpovědi každé rakety zvlášť
            for (unsigned char j = 0; j < ROCKETS_CNT; j++) {
                if (fgetc(pfFile) != ':') {
                    return ERR_PLAYFIELD;
                }
                int actChar = fgetc(pfFile);
                // let
                if (actChar == PF_ACT_CHAR[ACT_FLY]) {
                    pf->players[dest].rockets[j].lastAction = ACT_FLY;
                }
                // tažení asteroidu
                else if (actChar == PF_ACT_CHAR[ACT_PULL]) {
                    pf->players[dest].rockets[j].lastAction = ACT_PULL;
                }
                // střelba
                else if (actChar == PF_ACT_CHAR[ACT_SHOOT]) {
                    pf->players[dest].rockets[j].lastAction = ACT_SHOOT;
                }
                // bránění (zapnutí štítů)
                else if (actChar == PF_ACT_CHAR[ACT_DEFEND]) {
                    pf->players[dest].rockets[j].lastAction = ACT_DEFEND;
                    pf->players[dest].rockets[j].lastDirection = DIR_NODIR;
                }
                // chybná akce
                else if (actChar == PF_ACT_CHAR[ACT_ERROR]) {
                    pf->players[dest].rockets[j].lastAction = ACT_ERROR;
                    pf->players[dest].rockets[j].lastDirection = DIR_NODIR;
                }
                // jinak chyba
                else {
                    return ERR_PLAYFIELD;
                }
                // směr letu, tažení asteroidu nebo střelby
                if (actChar == PF_ACT_CHAR[ACT_FLY] ||
                        actChar == PF_ACT_CHAR[ACT_PULL] ||
                        actChar == PF_ACT_CHAR[ACT_SHOOT]) {
                    if (fgetc(pfFile) != ' ') {
                        return ERR_PLAYFIELD;
                    }
                    int dirChar = fgetc(pfFile);
                    // vlevo
                    if (dirChar == PF_DIR_CHAR[DIR_L]) {
                        pf->players[dest].rockets[j].lastDirection = DIR_L;
                    }
                    // vpravo
                    else if (dirChar == PF_DIR_CHAR[DIR_R]) {
                        pf->players[dest].rockets[j].lastDirection = DIR_R;
                    }
                    // nahoru
                    else if (dirChar == PF_DIR_CHAR[DIR_U]) {
                        pf->players[dest].rockets[j].lastDirection = DIR_U;
                    }
                    // dolů
                    else if (dirChar == PF_DIR_CHAR[DIR_D]) {
                        pf->players[dest].rockets[j].lastDirection = DIR_D;
                    }
                    // jinak chyba
                    else {
                        return ERR_PLAYFIELD;
                    }
                    // doplňující informace pro tažení asteroidu a střelbu
                    if (actChar == PF_ACT_CHAR[ACT_PULL] ||
                            actChar == PF_ACT_CHAR[ACT_SHOOT]) {
                        fscanf(pfFile, "%*1[ ](%*[^)])");
                    }
                    // možný let o dvě pole
                    if (actChar == PF_ACT_CHAR[ACT_FLY]) {
                        int tmp = fgetc(pfFile);
                        if (tmp == ' ') {
                            int dirChar2 = fgetc(pfFile);
                            // dvě pole vlevo
                            if (dirChar == PF_DIR_CHAR[DIR_L] &&
                                    dirChar2 == PF_DIR_CHAR[DIR_L]) {
                                pf->players[dest].rockets[j].lastDirection = DIR_LL;
                            }
                            // dvě pole vpravo
                            else if (dirChar == PF_DIR_CHAR[DIR_R] &&
                                     dirChar2 == PF_DIR_CHAR[DIR_R]) {
                                pf->players[dest].rockets[j].lastDirection = DIR_RR;
                            }
                            // dvě pole nahoru
                            else if (dirChar == PF_DIR_CHAR[DIR_U] &&
                                     dirChar2 == PF_DIR_CHAR[DIR_U]) {
                                pf->players[dest].rockets[j].lastDirection = DIR_UU;
                            }
                            // dvě pole dolů
                            else if (dirChar == PF_DIR_CHAR[DIR_D] &&
                                     dirChar2 == PF_DIR_CHAR[DIR_D]) {
                                pf->players[dest].rockets[j].lastDirection = DIR_DD;
                            }
                            // vlevo nahoru
                            else if ((dirChar == PF_DIR_CHAR[DIR_L] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_U]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_U] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_L])) {
                                pf->players[dest].rockets[j].lastAction = DIR_LU;
                            }
                            // vlevo dolů
                            else if ((dirChar == PF_DIR_CHAR[DIR_L] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_D]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_D] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_L])) {
                                pf->players[dest].rockets[j].lastAction = DIR_LD;
                            }
                            // vpravo nahoru
                            else if ((dirChar == PF_DIR_CHAR[DIR_R] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_U]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_U] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_R])) {
                                pf->players[dest].rockets[j].lastAction = DIR_RU;
                            }
                            // vpravo dolů
                            else if ((dirChar == PF_DIR_CHAR[DIR_R] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_D]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_D] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_R])) {
                                pf->players[dest].rockets[j].lastAction = DIR_RD;
                            }
                            // tam a zpět
                            else if ((dirChar == PF_DIR_CHAR[DIR_L] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_R]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_R] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_L]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_U] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_D]) ||
                                     (dirChar == PF_DIR_CHAR[DIR_D] &&
                                      dirChar2 == PF_DIR_CHAR[DIR_U])) {
                                pf->players[dest].rockets[j].lastDirection = DIR_NODIR;
                            }
                            // jinak nejde o let o dvě pole
                            else {
                                ungetc(dirChar2, pfFile);
                                ungetc(tmp, pfFile);
                            }
                        }
                        else {
                            ungetc(tmp, pfFile);
                        }
                    }
                }
            }
        }
    }
    // přechod na konec řádku
    fscanf(pfFile, "%*[ \t]");
    if (fgetc(pfFile) != '\n') {
        return ERR_PLAYFIELD;
    }
    // načtení rozmístění asteroidů
    pf->asteroids.cnt = pf->asteroids.maxCnt = 0;
    pf->asteroids.array = NULL;
    for (unsigned short y = 0; y < pf->height; y++) {
        for (unsigned short x = 0; x < pf->width; x++) {
            int weight = fgetc(pfFile);
            // žádný asteroid
            if (weight == ' ') {
                continue;
            }
            // asteroid s platnou váhou
            weight -= '0';
            if (weight > 0 && weight <= ROCKETS_CNT) {
                int errCode = addToAstArray(&pf->asteroids, x, y, weight);
                if (errCode != ERR_OK) {
                    freeAstArray(&pf->asteroids);
                    return errCode;
                }
            }
            // jinak chyba
            else {
                freeAstArray(&pf->asteroids);
                return ERR_PLAYFIELD;
            }
        }
        // přechod na nový řádek
        int tmp = fgetc(pfFile);
        if (tmp != '\n' && (tmp != EOF || y != pf->height - 1)) {
            freeAstArray(&pf->asteroids);
            return ERR_PLAYFIELD;
        }
    }
    // konec souboru
    if (fgetc(pfFile) != EOF) {
                freeAstArray(&pf->asteroids);
                return ERR_PLAYFIELD;
            }
    return ERR_OK;
}


/**
 * Načte hrací plán ze zadaného souboru a uloží jej do struktury typu
 * TPlayfield.
 * @param fileName Jméno souboru, ze kterého se bude hrací plán načítat.
 * @param myID ID hráče, za kterého strategie hraje.
 * @param pf Výsledný hrací plán. Při jakékoliv chybě je provedena pouze jeho
 *           inicializace. Hráč, za kterého strategie hraje, je v poli hráčů
 *           uložen na pozici 0, zbylí hráči jsou pak seřazeni vzestupně podle
 *           ID.
 * @return Při úspěšném načtení vrací ERR_OK, při problému s otevřením souboru
 *         ERR_FOPEN, při chybném formátu souboru ERR_PLAYFIELD, při neúspěšné
 *         alokaci paměti ERR_MEM.
 */
int readPlayfield(char *fileName, unsigned char myID, TPlayfield *pf) {
    int errCode;
    // otevření souboru
    FILE *pfFile;
    if ((pfFile = fopen(fileName, "r")) == NULL) {
        errCode = ERR_FOPEN;
    }
    // načtení hracího plánu
    else {
        errCode = _readPlayfield(pfFile, myID, pf);
        fclose(pfFile);
    }
    // výskyt chyby - inicializace struktury
    if (errCode) {
        pf->round = pf->totalRounds = 0;
        pf->width = pf->height = 0;
        // inicializace prvního hráče
        pf->players[0].points = 0;
        pf->players[0].base = (TCoords){0, 0};
        pf->players[0].rockets[0].coords = (TCoords){0,0};
        pf->players[0].rockets[0].lastAction = ACT_NOACT;
        pf->players[0].rockets[0].lastDirection = DIR_NODIR;
        for (unsigned char i = 1; i < ROCKETS_CNT; i++) {
            memcpy(&pf->players[0].rockets[i], &pf->players[0].rockets[0],
                    sizeof(TRocket));
        }
        // inicializace zbylých hráčů
        for (unsigned char i = 1; i < PLAYERS_CNT; i++) {
            memcpy(&pf->players[i], &pf->players[0], sizeof(TPlayer));
        }
        // inicializace pole asteroidů
        pf->asteroids.cnt = pf->asteroids.maxCnt = 0;
        pf->asteroids.array = NULL;
    }

    return errCode;
}


/**
 * Přidá asteroid do pole asteroidů.
 * @param astArray Pole asteroidů, do kterého má být asteroid přidán.
 * @param x Horizontální souřadnice asteroidu.
 * @param y Vertikální souřadnice asteroidu.
 * @param weight Váha asteroidu.
 * @return Při úspěšném přidání vrací ERR_OK, při neúspěšné alokaci paměti
 *         ERR_MEM.
 */
int addToAstArray(TAstArray *astArray, unsigned char x, unsigned char y,
                  unsigned char weight) {
    // případné zvětšení pole ukazatelů
    if (astArray->cnt == astArray->maxCnt) {
        TAsteroid **tmp = realloc(astArray->array, (astArray->maxCnt +
                                  AST_ARRAY_BS) * sizeof(TAsteroid*));
        if (tmp == NULL) {
            return ERR_MEM;
        }
        else {
            astArray->array = tmp;
            astArray->maxCnt += AST_ARRAY_BS;
        }
    }
    // vytvoření nového asteroidu
    if ((astArray->array[astArray->cnt] = malloc(sizeof(TAsteroid))) == NULL) {
        return ERR_MEM;
    }
    astArray->array[astArray->cnt]->coords.x = x;
    astArray->array[astArray->cnt]->coords.y = y;
    astArray->array[astArray->cnt]->weight = weight;
    astArray->cnt++;
    return ERR_OK;
}


/**
 * Zkopíruje pole asteroidů. Jedná se o mělkou kopii, kdy je zkopírováno pouze
 * pole ukazatelů na asteroidy.
 * @param dest Cílové pole asteroidů. Při neúspěšné alokaci paměti je provedena
 *             pouze jeho inicializace.
 * @param source Zdrojové pole asteroidů.
 * @return Při úspěšném zkopírování pole vrací ERR_OK, při neúspěšné alokaci
 *         paměti ERR_MEM.
 */
int copyAstArray(TAstArray *dest, TAstArray *source) {
    dest->cnt = source->cnt;
    dest->maxCnt = source->maxCnt;
    if ((dest->array = malloc(dest->maxCnt * sizeof(TAsteroid*))) == NULL) {
        dest->cnt = dest->maxCnt = 0;
        dest->array = NULL;
        return ERR_MEM;
    }
    memcpy(dest->array, source->array, dest->maxCnt * sizeof(TAsteroid*));
    return ERR_OK;
}


/**
 * Uvolní dynamicky alokované pole asteroidů. Počet asteroidů a maximální počet
 * asteroidů nastaví na 0. Ukazatel na pole nastaví na NULL.
 * @param astArray Pole asteroidů. které má být uvolněno.
 */
void freeAstArray(TAstArray *astArray) {
    for (unsigned int i = 0; i < astArray->cnt; i++) {
        free(astArray->array[i]);
    }
    free(astArray->array);
    astArray->cnt = astArray->maxCnt = 0;
    astArray->array = NULL;
}


/**
 * Určí vzdálenost dvou bodů.
 * @param a Souřadnice prvního bodu.
 * @param b Souřadnice druhého bodu.
 * @return Vrací vzdálenost zadaných bodů.
 */
unsigned short getDistance(TCoords a, TCoords b) {
    return abs(a.x - b.x) + abs(a.y - b.y);
}


/**
 * Seřadí asteroidy vzestupně podle vzdálenosti od zadaného bodu.
 * array Asteroidy k seřazení.
 * point Bod, od kterého je počítána vzdálenost.
 * left Index prvního asteroidu k seřazení.
 * right Index posledního asteroidu k seřazení.
 */
void sortAstsByDist(TAsteroid **array, TCoords point, unsigned short left,
                    unsigned short right) {
    // není co řadit
    if (left >= right) {
        return;
    }
    // řazení metodou quick sort
    int i = left;
    int j = right;
    unsigned short pivot = getDistance(point, array[(i + j) / 2]->coords) * 10 + array[(i + j) / 2]->weight;
    do {
        while (getDistance(point, array[i]->coords) * 10 + array[i]->weight < pivot) {
            i++;
        }
        while (getDistance(point, array[j]->coords) * 10 + array[j]->weight > pivot) {
            j--;
        }
        if (i <= j) {
            TAsteroid *tmp = array[i];
            array[i++] = array[j];
            array[j--] = tmp;
        }
    } while (i <= j);
    if (left < j) {
        sortAstsByDist(array, point, left, j);
    }
    if (right > i) {
        sortAstsByDist(array, point, i, right);
    }
}


/**
 * Určí možné směry letu rakety, kterými bude dosaženo největšího přiblížení
 * k zadanému cíli.
 * @param directions Získané směry letu.
 * @param rocket Souřadnice rakety.
 * @param dest Souřadnice cíle letu.
 * @return Vrací počet získaných směrů letu. Největšího přiblížení k cíli může
 *         být dosaženo vždy maximálně 3 (BEST_FLY_DIRS_CNT) různými směry letu.
 */
int getFlyDirs(int *directions, TCoords rocket, TCoords dest) {
    int cnt = 0;
    short distX = dest.x - rocket.x;
    short distY = dest.y - rocket.y;
    // dvě pole vlevo
    if (distX <= -2) {
        directions[cnt++] = DIR_LL;
    }
    // dvě pole vpravo
    else if (distX >= 2) {
        directions[cnt++] = DIR_RR;
    }
    // dvě pole nahoru
    if (distY <= -2) {
        directions[cnt++] = DIR_UU;
    }
    // dvě pole dolů
    else if (distY >= 2) {
        directions[cnt++] = DIR_DD;
    }
    // vlevo nahoru
    if (distX == -1 && distY == -1) {
        directions[cnt++] = DIR_LU;
    }
    // vlevo dolů
    else if (distX == -1 && distY == 1) {
        directions[cnt++] = DIR_LD;
    }
    // vpravo nahoru
    else if (distX == 1 && distY == -1) {
        directions[cnt++] = DIR_RU;
    }
    // vpravo dolů
    else if (distX == 1 && distY == 1) {
        directions[cnt++] = DIR_RD;
    }
    // další směry letu vedou k menšímu přiblížení
    if (cnt > 0) {
        return cnt;
    }
    // vlevo
    if (distX == -1) {
        directions[cnt++] = DIR_L;
    }
    // vpravo
    else if (distX == 1) {
        directions[cnt++] = DIR_R;
    }
    // nahoru
    if (distY == -1) {
        directions[cnt++] = DIR_U;
    }
    // dolů
    else if (distY == 1) {
        directions[cnt++] = DIR_D;
    }
    return cnt;
}


/**
 * Určí možné směry tažení asteroidu, kterými bude dosaženo největšího
 * přiblížení k domovské základně.
 * @param directions Získané směry tažení asteroidu.
 * @param rocket Souřadnice rakety.
 * @param dest Souřadnice domovské základny.
 * @return Vrací počet získaných směrů tažení asteroidu. Největšího přiblížení
 *         k domovské základně může být dosaženo vždy maximálně 2
 *         (BEST_PULL_DIRS_CNT) různými směry tažení asteroidu.
 */
int getPullDirs(int *directions, TCoords rocket, TCoords base) {
    int cnt = 0;
    short distX = base.x - rocket.x;
    short distY = base.y - rocket.y;
    // vlevo
    if (distX <= -1) {
        directions[cnt++] = DIR_L;
    }
    // vpravo
    else if (distX >= 1) {
        directions[cnt++] = DIR_R;
    }
    // nahoru
    if (distY <= -1) {
        directions[cnt++] = DIR_U;
    }
    // dolů
    else if (distY >= 1) {
        directions[cnt++] = DIR_D;
    }
    return cnt;
}


/**
 * Vytiskne na standardní výstup odpověď programu.
 * @param actR1 Akce první rakety.
 * @param dirR1 Směr, kterým je akce první rakety prováděna.
 * @param actR2 Akce druhé rakety.
 * @param dirR2 Směr, kterým je akce druhé rakety prováděna.
 */
void printAnswer(int actR1, int dirR1, int actR2, int dirR2) {
    printf("%c%s:%c%s\n", ANSW_ACT_CHAR[actR1], ANSW_DIR_STR[dirR1],
                          ANSW_ACT_CHAR[actR2], ANSW_DIR_STR[dirR2]);
}
