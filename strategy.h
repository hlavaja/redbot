/*
 * Projekt: Herní strategie pro soutěž RedBot 2013 podzim
 *          http://red-bot.rhcloud.com/node/57
 * Autor:   Jan Hlaváč, hlavaja@gmail.com
 * Datum:   2013-12-02
 * Soubor:  strategy.h
 * Popis:   Definice symbolických konstant a prototypy funkcí určených pro
 *          nalezení nejlepšího tahu.
 */

#ifndef STRATEGY_H
#define STRATEGY_H


/** Pozice první rakety v poli raket */
#define R1 0

/** Pozice druhé rakety v poli raket */
#define R2 1


int strategy(TPlayfield *pf, int *actions, int *directions);


#endif
