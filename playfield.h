/*
 * Projekt: Herní strategie pro soutěž RedBot 2013 podzim
 *          http://red-bot.rhcloud.com/node/57
 * Autor:   Jan Hlaváč, hlavaja@gmail.com
 * Datum:   2013-12-02
 * Soubor:  playfield.h
 * Popis:   Definice struktur a prototypy funkcí určených pro práci s hracím
 *          plánem.
 */

#ifndef PLAYFIELD_H
#define PLAYFIELD_H


/** Pozice mého týmu v poli hráčů uvnitř struktury typu TPlayfield */
#define MY_TEAM 0

/** Počet hráčů */
#define PLAYERS_CNT 2

/** Počet raket, které má každý hráč k dispozici */
#define ROCKETS_CNT 2

/** Velikost bloků, po kterých je pole asteroidů alokováno */
#define AST_ARRAY_BS 10

/** Maximální počet různých směrů letu rakety, kterými bude dosaženo největšího
 *  přiblížení k zadanému cíli. */
#define BEST_FLY_DIRS_CNT 3

/** Maximální počet různých směrů tažení asteroidu, kterými bude dosaženo
 *  největšího přiblížení k domovské základně. */
#define BEST_PULL_DIRS_CNT 2


/** Akce, které mohou být raketou provedeny nebo které byly provedeny
 *  v předchozím kole. */
enum tactions {
    ACT_FLY = 0,    /**< Let */
    ACT_PULL,       /**< Tažení asteroidu */
    ACT_SHOOT,      /**< Střelba */
    ACT_DEFEND,     /**< Bránění (zapnutí štítů) */
    ACT_ERROR,      /**< Chybná akce */
    ACT_NOACT       /**< Žádná akce */
};


/** Možné směry letu rakety. První 4 směry platí i pro tažení asteroidu
 *  a střelbu. */
enum tdirections {
    DIR_L = 0,      /**< Vlevo */
    DIR_R,          /**< Vpravo */
    DIR_U,          /**< Nahoru */
    DIR_D,          /**< Dolů */
    DIR_LL,         /**< Dvě pole vlevo */
    DIR_RR,         /**< Dvě pole vpravo */
    DIR_UU,         /**< Dvě pole nahoru */
    DIR_DD,         /**< Dvě pole dolů */
    DIR_LU,         /**< Vlevo nahoru*/
    DIR_LD,         /**< Vlevo dolů */
    DIR_RU,         /**< Vpravo nahoru */
    DIR_RD,         /**< Vpravo dolů */
    DIR_NODIR       /**< Žádný směr */
};


/**
 * Struktura reprezentující souřadnice bodu v dvourozměrném prostoru.
 */
typedef struct {
    unsigned char x;                /**< Horizontální osa */
    unsigned char y;                /**< Vertikální osa */
} TCoords;


/**
 * Struktura sdružující informace o raketě.
 */
typedef struct {
    TCoords coords;                 /**< Souřadnice rakety */
    int lastAction;                 /**< Poslední akce rakety */
    int lastDirection;              /**< Směr poslední akce */
} TRocket;


/**
 * Struktura sdružující informace o hráči.
 */
typedef struct {
    unsigned int points;            /**< Počet bodů */
    TCoords base;                   /**< Souřadnice základny */
    TRocket rockets[ROCKETS_CNT];   /**< Informace o raketách */
} TPlayer;


/**
 * Struktura sdružující informace o asteroidu.
 */
typedef struct {
    TCoords coords;                 /**< Souřadnice asteroidu */
    unsigned char weight;           /**< Váha asteroidu */
} TAsteroid;


/**
 * Struktura reprezentující pole asteroidů.
 */
typedef struct {
    unsigned int cnt;               /**< Počet uložených asteroidů */
    unsigned int maxCnt;            /**< Maximální počet asteroidů */
    TAsteroid **array;              /**< Pole asteroidů */
} TAstArray;


/**
 * Struktura sdružující informace o hracím plánu.
 */
typedef struct {
    unsigned int round;             /**< Aktuální kolo */
    unsigned int totalRounds;       /**< Celkový počet kol */
    unsigned char width;            /**< Šířka hracího plánu */
    unsigned char height;           /**< Výška hracího plánu */
    TPlayer players[PLAYERS_CNT];   /**< Hráči */
    TAstArray asteroids;            /**< Asteroidy */
} TPlayfield;


/**
 * Struktura pro pohyb obecným směrem v hracím plánu.
 */
typedef struct {
    short dX;       /**< Rozdíl horizontální ose */
    short dY;       /**< Rozdíl ve vertikální ose */
} TIncrement;


// Prototypy funkcí
int readPlayfield(char *fileName, unsigned char myID, TPlayfield *pf);
int addToAstArray(TAstArray *astArray, unsigned char x, unsigned char y,
                  unsigned char weight);
int copyAstArray(TAstArray *dest, TAstArray *source);
void freeAstArray(TAstArray *astArray);
unsigned short getDistance(TCoords a, TCoords b);
void sortAstsByDist(TAsteroid **array, TCoords point, unsigned short left,
                    unsigned short right);
int getFlyDirs(int *directions, TCoords rocket, TCoords dest);
int getPullDirs(int *directions, TCoords rocket, TCoords base);
void printAnswer(int actR1, int dirR1, int actR2, int dirR2);

#endif
