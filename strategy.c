/*
 * Projekt: Herní strategie pro soutěž RedBot 2013 podzim
 *          http://red-bot.rhcloud.com/node/57
 * Autor:   Jan Hlaváč, hlavaja@gmail.com
 * Datum:   2013-12-01
 * Soubor:  strategy.c
 * Popis:   Funkce určené pro nalezení nejlepšího tahu.
 */

#include <stdio.h>
#include <stdlib.h>
#include "defines.h"
#include "playfield.h"
#include "strategy.h"

extern const TIncrement DIR_INC[];


/**
 * Nalezne nejlepší tah pro zadaný hrací plán.
 * @param pf Hrací plán, pro který se hledá nejlepší tah.
 * @param actions Výsledné akce jednotlivých raket.
 * @param directionR1 Směry jednotlivých výsledných akcí.
 * @return Při úspěšném nalezení nejlepšího tahu vrací ERR_OK, při neúspěšné
 *         alokaci paměti ERR_MEM.
 */
int strategy(TPlayfield *pf, int *actions, int *directions) {
    int errCode;
    TRocket *rockets = pf->players[MY_TEAM].rockets;
    TAstArray astArrays[ROCKETS_CNT];
    // pole asteroidů seřazené vzestupně podle vzdálenosti od první rakety
    if ((errCode = copyAstArray(&astArrays[R1], &pf->asteroids)) != ERR_OK) {
        return errCode;
    }
    sortAstsByDist(astArrays[R1].array, rockets[R1].coords, 0,
                   astArrays[R1].cnt - 1);
    // pole asteroidů seřazené vzestupně podle vzdálenosti od druhé rakety
    if ((errCode = copyAstArray(&astArrays[R2], &pf->asteroids)) != ERR_OK) {
        free(astArrays[R1].array);
        return errCode;
    }
    sortAstsByDist(astArrays[R2].array, rockets[R2].coords, 0,
                   astArrays[R2].cnt - 1);


    actions[R1] = actions[R2] = ACT_DEFEND;
    directions[R1] = directions[R2] = DIR_NODIR;

    TAsteroid *destR1 = astArrays[R1].array[0];
    TAsteroid *destR2 = astArrays[R2].array[0];

    // stejný cíl s váhou 1 - jeden z cílů je nahrazen novým
    if (destR1 == destR2 && destR1->weight == 1) {
        // první raketa je k cíli blíže - nový cíl pro druhou raketu
        if (getDistance(destR1->coords, rockets[R1].coords) <=
                getDistance(destR2->coords, rockets[R2].coords)) {
            if (astArrays[R2].cnt >= 2) {
                destR2 = astArrays[R2].array[1];
            }
            else {
                destR2 = NULL;
            }
        }
        // druhá raketa je k cíli blíže - nový cíl pro první raketu
        else {
            if (astArrays[R1].cnt >= 2) {
                destR1 = astArrays[R1].array[1];
            }
            else {
                destR1 = NULL;
            }
        }
    }
    // rozdílné cíle a druhá raketa je blíže k cíli s váhou 2 - přivolání první
    // rakety
    else if (destR1 != destR2 &&
            getDistance(destR2->coords, rockets[R2].coords) <
            getDistance(destR1->coords, rockets[R1].coords)) {
        destR1 = destR2;
    }
    // rozdílné cíle a první raketa je blíže k cíli s váhou 2 - přivolání druhé
    // rakety
    else if (destR1 != destR2 &&
            getDistance(destR1->coords, rockets[R1].coords) <
            getDistance(destR2->coords, rockets[R2].coords)) {
        destR2 = destR1;
    }


    // obě rakety táhnou asteroid
    if (destR1 != NULL && destR2 != NULL &&
            getDistance(destR1->coords, rockets[R1].coords) == 0 &&
            getDistance(destR2->coords, rockets[R2].coords) == 0) {
        // každá táhne jiný asteroid
        if (destR1 != destR2) {
            int bestDirsR1[BEST_PULL_DIRS_CNT];
            int dirsCntR1 = getPullDirs(bestDirsR1, rockets[R1].coords,
                                        pf->players[MY_TEAM].base);
            int bestDirsR2[BEST_PULL_DIRS_CNT];
            int dirsCntR2 = getPullDirs(bestDirsR2, rockets[R2].coords,
                                        pf->players[MY_TEAM].base);
            directions[R1] = directions[R2] = DIR_NODIR;
            for (unsigned char i = 0; i < dirsCntR1; i++) {
                for (unsigned char j = 0; j < dirsCntR2; j++) {
                    if (getDistance((TCoords){rockets[R1].coords.x + DIR_INC[bestDirsR1[i]].dX,
                                              rockets[R1].coords.y + DIR_INC[bestDirsR1[i]].dY},
                                    (TCoords){rockets[R2].coords.x + DIR_INC[bestDirsR2[j]].dX,
                                              rockets[R2].coords.y + DIR_INC[bestDirsR2[j]].dY}) != 0) {
                        actions[R1] = actions[R2] = ACT_PULL;
                        directions[R1] = bestDirsR1[i];
                        directions[R2] = bestDirsR2[j];
                        break;
                    }
                }
                if (directions[R1] != DIR_NODIR) {
                    break;
                }
            }
            if (directions[R1] == DIR_NODIR) {
                actions[R1] = ACT_PULL;
                directions[R1] = bestDirsR1[0];
                actions[R2] = ACT_DEFEND;
            }

        }
        // společně táhnou jeden asteroid
        else {
            int bestDirs[BEST_PULL_DIRS_CNT];
            getPullDirs(bestDirs, rockets[R1].coords,
                        pf->players[MY_TEAM].base);
            actions[R1] = ACT_PULL;
            directions[R1] = bestDirs[0];
            actions[R2] = ACT_PULL;
            directions[R2] = bestDirs[0];
        }
    }
    else {
        // žádný cíl pro první raketu
        if (destR1 == NULL) {
            actions[R1] = ACT_DEFEND;
            directions[R1] = DIR_NODIR;
        }
        // první raketa táhne asteroid
        else if (getDistance(destR1->coords, rockets[R1].coords) == 0) {
            int bestDirs[BEST_PULL_DIRS_CNT];
            getPullDirs(bestDirs, rockets[R1].coords,
                        pf->players[MY_TEAM].base);
            actions[R1] = ACT_PULL;
            directions[R1] = bestDirs[0];
        }
        // první raketa letí
        else {
            int bestDirs[BEST_FLY_DIRS_CNT];
            getFlyDirs(bestDirs, rockets[R1].coords, destR1->coords);
            actions[R1] = ACT_FLY;
            directions[R1] = bestDirs[0];
        }
        // žádný cíl pro druhou raketu
        if (destR2 == NULL) {
            actions[R2] = ACT_DEFEND;
            directions[R2] = DIR_NODIR;
        }
        // druhá raketa táhne asteroid
        else if (getDistance(destR2->coords, rockets[R2].coords) == 0) {
            int bestDirs[BEST_PULL_DIRS_CNT];
            getPullDirs(bestDirs, rockets[R2].coords,
                        pf->players[MY_TEAM].base);
            actions[R2] = ACT_PULL;
            directions[R2] = bestDirs[0];
        }
        // druhá raketa letí
        else {
            int bestDirs[BEST_FLY_DIRS_CNT];
            getFlyDirs(bestDirs, rockets[R2].coords, destR2->coords);
            actions[R2] = ACT_FLY;
            directions[R2] = bestDirs[0];
        }
    }

    free(astArrays[R1].array);
    free(astArrays[R2].array);

    return ERR_OK;
}
