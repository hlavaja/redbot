/*
 * Projekt: Herní strategie pro soutěž RedBot 2013 podzim
 *          http://red-bot.rhcloud.com/node/57
 * Autor:   Jan Hlaváč, hlavaja@gmail.com
 * Datum:   2013-12-02
 * Soubor:  daedalus.c
 * Popis:   Hlavní tělo programu.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defines.h"
#include "playfield.h"
#include "strategy.h"


/** Jméno souboru s hracím plánem */
#define PLAYFIELD_FILE "playfield.txt"


/** Režimy programu */
enum tmodes {
    MODE_HELP,      /**< Nápověda */
    MODE_STRATEGY   /**< Strategie */
};


/** Chybové zprávy odpovídající výčtu terrcodes v defines.h */
const char *ERR_MSG[] = {
    [ERR_OK]        = "Vse v poradku",
    [ERR_ARGS]      = "Chybne argumenty prikazoveho radku",
    [ERR_FOPEN]     = "Nepodarilo se otevrit soubor s hracim planem",
    [ERR_PLAYFIELD] = "Neplatny format hraciho planu",
    [ERR_MEM]       = "Nepodarilo se alokovat potrebnou pamet",
    [ERR_MODE]      = "Neznamy rezim programu",
    [ERR_UNKNOWN]   = "Neznama chyba"
};


/** Text nápovědy */
const char HELP_MSG[] =
    "Herni strategie pro soutez RedBot 2013 podzim\n"
    "(http://red-bot.rhcloud.com/node/57)\n"
    "\n"
    "Autor:      Jan Hlavac, hlavaja@gmail.com, 2013-12-02\n"
    "\n"
    "Pouziti:    daedalus -h\n"
    "            daedalus 2\n"
    "\n"
    "Popis argumentu:\n"
    " -h         Zobrazi tuto napovedu.\n"
    " ID         ID hrace, za ktereho strategie hraje. Muze mit hodnotu 1 nebo 2.\n"
;


/**
 * Struktura sdružující argumenty příkazového řádku.
 */
typedef struct {
    int mode;           /**< Režim programu odpovídající vyčtu tmodes */
    unsigned char myID; /**< ID hráče, za kterého strategie hraje */
} TArgs;


/**
 * Zpracuje argumenty příkazového řádku a uloží je do struktury typu TArgs.
 * @param argc Počet argumentů příkazového řádku.
 * @param argv Pole textových řetězců s argumenty příkazového řádku.
 * @param args Získané argumenty příkazového řádku.
 * @return Při úspěšném získání argumentů příkazového řádku vrací ERR_OK, při
 *         výskytu chyby ERR_ARGS.
 */
int getArgs(int argc, char *argv[], TArgs *args) {
    // 1 argument
    if (argc == 2) {
        // nápověda
        if (strcmp("-h", argv[1]) == 0) {
            args->mode = MODE_HELP;
        }
        // ID hráče
        else if (argv[1][1] == '\0' && argv[1][0] >= '1' && argv[1][0] <= '2') {
            args->mode = MODE_STRATEGY;
            args->myID = argv[1][0] - '0';
        }
        // jinak chyba
        else {
            return ERR_ARGS;
        }
    }
    // jinak chyba
    else {
        return ERR_ARGS;
    }

    return ERR_OK;
}


/**
 * Vytiskne na chybový výstup zprávu odpovídající konkrétnímu chybovému kódu
 * programu.
 * @param errCode Chybový kód programu odpovídající výčtu terrcodes.
 */
void printError(int errCode) {
    if (errCode < ERR_OK || errCode > ERR_UNKNOWN) {
        errCode = ERR_UNKNOWN;
    }
    fprintf(stderr, "Chyba: %s\n",ERR_MSG[errCode]);
}


/**
 * Hlavní program.
 */
int main(int argc, char *argv[]) {
    int errCode = ERR_OK;   // chybový kód programu

    // získání argumentů příkazového řádku
    TArgs args;
    if ((errCode = getArgs(argc, argv, &args)) != ERR_OK) {
        printError(errCode);
        return EXIT_FAILURE;
    }

    // nápověda
    if (args.mode == MODE_HELP) {
        printf(HELP_MSG);
    }

    // strategie
    else if (args.mode == MODE_STRATEGY) {
        TPlayfield pf;
        // načtení hracího plánu
        if ((errCode = readPlayfield(PLAYFIELD_FILE, args.myID, &pf)) !=
                ERR_OK) {
            printError(errCode);
            return EXIT_FAILURE;
        }
        // nalezení nejlepšího tahu
        int actions[ROCKETS_CNT], directions[ROCKETS_CNT];
        if ((errCode = strategy(&pf, actions, directions)) !=
                ERR_OK) {
            printError(errCode);
            freeAstArray(&pf.asteroids);
            return EXIT_FAILURE;
        }
        freeAstArray(&pf.asteroids);
        // tisk odpovědi
        printAnswer(actions[0], directions[0], actions[1], directions[1]);
    }

    // neznámý režim programu
    else {
        printError(ERR_MODE);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
